const { expect } = require('chai');

const { quicksort } = require('./solution');


describe('quicksort', () => {
    it('sorts an empty arr', () => {
        const res = quicksort([]);

        expect(res).to.deep.equal([]);
    });

    it('sorts an arr of 1', () => {
        const res = quicksort([1]);

        expect(res).to.deep.equal([1]);
    });

    it('sorts an even length arr', () => {
        const arr1 = [456, 99, 5, 3, 90, 7, 443, -23, -56, 2, 3, 1, -6, 789];
        const actual = quicksort(arr1);

        const expected = [...arr1].sort((a, b) => (a > b ? 1 : -1))
        expect(actual).to.deep.equal(expected);
    });

    it('sorts an odd length arr', () => {
        const arr2 = [45, 67, 879, 34, -545, 0, -234, 6, 8, 34, 0, -23, 9];
        const actual = quicksort(arr2);

        const expected = [...arr2].sort((a, b) => (a > b ? 1 : -1))
        expect(actual).to.deep.equal(expected);
    });
});
