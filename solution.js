
/*
pseudo code
1. break case (because recursion)
2. pick a pivot (end)
3. find smaller and larger numbers
4. recurse smaller, larger
5. stitch the sorted sub arrays back together around the pivot
*/


function quicksort(arr) {
    if (arr.length <= 1) { return arr; }

    const pivot = arr[arr.length - 1];
    const left = [];
    const right = [];

    for (let i = 0; i < arr.length - 1; i += 1) {
        if (arr[i] > pivot) {
            right.push(arr[i]);
        } else { // if (arr[i] <= pivot)
            left.push(arr[i]);
        }
    }

    const sortedLeft = quicksort(left);
    const sortedRight = quicksort(right);

    const res = [...sortedLeft, pivot, ...sortedRight]
    return res;
}

module.exports = {
    quicksort
}
