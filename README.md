## description
here's my tests and solution to quicksort. an n * log(n), divide and conquer sorting algorithm

this is also a repo that you can copy, to create your own simple algo test environment, or follow the instructions below to set it up from scratch

## file structure set up
1. create the root folder `mkdir quicksort`
2. create the test and solution files `touch solution.js` and `touch test.js`

## npm set up
1. start `npm init`
2. fill out the terminal form (not much of them really matter apart from the "test script". set that to `mocha`)
3. add the testing libraries `npm install mocha chai`

## run instructions
1. `npm install`
2. `npm run test`

